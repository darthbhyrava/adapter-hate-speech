""" Script to replicate single adapter experiments
"""

import argparse
import logging
from pathlib import Path

from adapter_models.single_adapters import SingleAdaptersExperiment
from dataset_processing.hasoc_dataset_processor import HASOCDatasetProcessor
from dataset_processing.olid_dataset_processor import OLIDDatasetProcessor


def parse_args() -> argparse.Namespace:
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', '-d',
                        choices=['hasoc', 'olid'],
                        default='Which dataset to run experiments on.')
    parser.add_argument('--task_ids', '-t',
                        nargs='+',
                        default='Tasks for which to create individual adapters. '
                                'For hasoc use 1/2/3 '
                                'For olid use a/b/c ')
    parser.add_argument('--base_output_dir', '-bo',
                        default='single_adapter_experiment_runs')
    parser.add_argument('--model_name',
                        default='bert-base-uncased')
    args = parser.parse_args()

    return args


def setup_logging(log_file: Path, app_name: str, log_level: int = logging.INFO) -> logging.Logger:
    """ Setting up logging. Log lines will be formatted to have below segments:
        - application name
        - timestamp
        - log level
        - location of log statement (file, function, line)
        - log message

    :param log_file: Path to file where to write logs to.
    :param app_name: application name
    :param log_level: log level

    :return: logger to record messages
    """
    # Create parent log directory
    log_file.parent.mkdir(parents=True, exist_ok=True)

    # Create log formatter
    log_format = app_name + " [%(asctime)s] %(levelname)s [%(filename)s:%(funcName)s:%(lineno)d] %(message)s"
    log_formatter = logging.Formatter(log_format)

    # Instantiate handlers
    file_handler = logging.FileHandler(log_file, mode='a')
    file_handler.setFormatter(log_formatter)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)

    logging.basicConfig(handlers=[file_handler, console_handler], level=log_level)

    return logging.getLogger()


def main():
    args = parse_args()

    dataset = args.dataset
    logger = setup_logging(log_file=Path(f'{dataset}_sa_experiments_{args.model_name}.log'),
                           app_name='Single Adapter Experiments')

    for task in args.task_ids:
        logger.info(f'Task: {task}')
        adapter_name = f'{dataset}_{task}'

        output_dir = Path(args.base_output_dir) / adapter_name / f'{args.model_name}'
        output_dir.mkdir(exist_ok=True, parents=True)

        if dataset == 'hasoc':
            dataset_preprocessor = HASOCDatasetProcessor()
        else:
            dataset_preprocessor = OLIDDatasetProcessor()

        sa_experiment_runner = SingleAdaptersExperiment(adapter_name=adapter_name,
                                                        dataset_preprocessor=dataset_preprocessor,
                                                        task_id=task, transformer_model_name=args.model_name,
                                                        output_dir=output_dir,
                                                        tokenizer_kwargs={'max_length': 80,
                                                                          'padding': 'max_length',
                                                                          'truncation': True})
        sa_experiment_runner.run_single_adapter_experiment()


if __name__ == '__main__':
    main()
