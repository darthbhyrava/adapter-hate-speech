from setuptools import find_packages, setup

# Read the contents of your README file
from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(name='SriharshDeepLearningProject',
      version='1.0.0',
      author='Sriharsh Bhyravajjula',
      author_email='hbhyrava@gmail.com',
      packages=find_packages(where="src", exclude=("test",)),
      package_dir={"": "src"},
      scripts=[],
      classifiers=['Programming Language :: Python :: 3'],
      description='Package for Multi-task Hate Speech Detection using Adapters',
      long_description=long_description,
      long_description_content_type='text/markdown',
      python_requires='>=3.7',
      install_requires=[
          "jupyterlab == 4.0.1",
          "ipykernel == 6.23.1",
          "nb_conda_kernels == 2.3.1",
          "numpy == 1.23.5",
          "pandas == 1.5.0",
          "scikit-learn == 1.2.2",
          "tqdm == 4.65.0",
          "transformers == 4.27.3",
          "datasets ==  2.12.0",
          "adapter-transformers == 3.2.1",
      ],)
