""" Run an experiment to add a single sequence classification task specific adapter and train it.
"""

from datasets import load_metric, Dataset
import json
import logging
import numpy as np
from pathlib import Path
from transformers import AutoConfig, AutoTokenizer, AutoModelWithHeads
from transformers import TrainingArguments, AdapterTrainer, EvalPrediction
from typing import Dict, Optional, Union
import torch

from dataset_processing.base_dataset_processor import BaseDatasetProcessor

logger = logging.getLogger(__name__)


class SingleAdaptersExperiment:
    def __init__(self, adapter_name: str, dataset_preprocessor: BaseDatasetProcessor, task_id: Union[str, int],
                 transformer_model_name: str, tokenizer_kwargs: Dict,
                 output_dir: Path,
                 training_arguments_json: Path = Path(__file__).parent.joinpath('default_adapter_training_arguments.json')):
        self.adapter_name = adapter_name
        self.dataset_preprocessor = dataset_preprocessor
        self.task_id = task_id

        self.transformer_model_name: str = transformer_model_name

        self.tokenizer_kwargs: Dict = tokenizer_kwargs
        self.tokenizer: Optional[AutoTokenizer] = None

        self.id2label: Optional[Dict[int, str]] = None

        self.transformer_config: Optional[AutoConfig] = None
        self.model: Optional[AutoModelWithHeads] = None

        self.output_dir = output_dir
        with open(training_arguments_json, 'r') as infile:
            self.training_arguments = json.load(fp=infile)
            self.training_arguments['output_dir'] = str(output_dir.resolve())

    def _load_tokenizer(self):
        """ Load a pre-trained transformer tokenizer
        """
        logger.info(f'Loading tokenizer: {self.transformer_model_name}')
        self.tokenizer = AutoTokenizer.from_pretrained(self.transformer_model_name)

    def _process_datasets(self):
        """ Tokenize datasets
        """
        logger.info('Processing datasets')
        datasets_dict = self.dataset_preprocessor.get_datasets(self.task_id)

        # Encode train dataset
        datasets_dict['train'] = datasets_dict['train'].map(self.dataset_preprocessor.encode_batch,
                                                            batched=True,
                                                            fn_kwargs={'tokenizer': self.tokenizer,
                                                                       'tokenizer_kwargs': self.tokenizer_kwargs})

        # Encode train dataset
        datasets_dict['val'] = datasets_dict['val'].map(self.dataset_preprocessor.encode_batch,
                                                        batched=True,
                                                        fn_kwargs={'tokenizer': self.tokenizer,
                                                                   'tokenizer_kwargs': self.tokenizer_kwargs})

        # Encode train dataset
        datasets_dict['test'] = datasets_dict['test'].map(self.dataset_preprocessor.encode_batch,
                                                          batched=True,
                                                          fn_kwargs={'tokenizer': self.tokenizer,
                                                                     'tokenizer_kwargs': self.tokenizer_kwargs})

        self.id2label = {id: label for (id, label) in enumerate(datasets_dict["train"].features["labels"].names)}
        logger.info(f'id2label: {self.id2label}')

        return datasets_dict

    def _load_pretrained_transformer_model(self):
        """ Load a pre-trained transformer config and model
        """
        if self.id2label is None:
            raise ValueError(f'id2label is empty. Please run process_datasets before loading the transformer config.')

        logger.info(f'Loading transformer config: {self.transformer_model_name}')
        self.transformer_config = AutoConfig.from_pretrained(self.transformer_model_name,
                                                             id2label=self.id2label)
        logger.info(f'Transformer config: {self.transformer_config}')

        logger.info(f'Loading transformer: {self.transformer_model_name}')
        self.model = AutoModelWithHeads.from_pretrained(self.transformer_model_name,
                                                        config=self.transformer_config,
                                                        )

    def _add_adapter(self):
        """ Add a task specific adapter with matching sequence classification heads.
        """
        # Add a new adapter
        self.model.add_adapter(self.adapter_name)

        # Add a matching classification head
        self.model.add_classification_head(self.adapter_name, num_labels=len(self.id2label), id2label=self.id2label)

        # Activate the adapter
        self.model.train_adapter(self.adapter_name)

    @staticmethod
    def _compute_metrics(eval_pred: EvalPrediction) -> Dict:
        metric1 = load_metric("precision")
        metric2 = load_metric("recall")
        metric3 = load_metric("f1")
        metric4 = load_metric("accuracy")

        logits, labels = eval_pred
        predictions = np.argmax(logits, axis=-1)

        precision_macro = metric1.compute(predictions=predictions, references=labels, average="macro")["precision"]
        recall_macro = metric2.compute(predictions=predictions, references=labels, average="macro")["recall"]
        f1_macro = metric3.compute(predictions=predictions, references=labels, average="macro")["f1"]
        precision_weighted = metric1.compute(predictions=predictions, references=labels, average="weighted")["precision"]
        recall_weighted = metric2.compute(predictions=predictions, references=labels, average="weighted")["recall"]
        f1_weighted = metric3.compute(predictions=predictions, references=labels, average="weighted")["f1"]
        accuracy = metric4.compute(predictions=predictions, references=labels)["accuracy"]

        return {"precision_macro": precision_macro, "recall_macro": recall_macro, "f1_macro": f1_macro,
                "accuracy": accuracy,
                "precision_weighted": precision_weighted, "recall_weighted": recall_weighted,
                "f1_weighted": f1_weighted}

    def _train_adapter(self, dataset_dict: Dict[str, Dataset]):
        # Initialize training arguments
        logger.info(f'Initializing training arguments')
        training_args = TrainingArguments(**self.training_arguments)

        # Initialize trainer
        logger.info(f'Initializing trainer')
        trainer = AdapterTrainer(model=self.model, args=training_args,
                                 train_dataset=dataset_dict["train"],
                                 eval_dataset=dataset_dict["val"],
                                 compute_metrics=self._compute_metrics)

        # Train
        logger.info(f'Training adapter')
        trainer.train()

        # Predict on test dataset
        logger.info(f'Predicting on test set')
        test_predictions, test_label_ids, test_metrics = trainer.predict(dataset_dict['test'])
        np.save(arr=test_predictions, file=self.output_dir / 'test_predictions.npy')
        np.save(arr=test_label_ids, file=self.output_dir / 'test_label_ids.npy')
        logger.info(f'Test metrics: {test_metrics}')
        with open(self.output_dir / 'test_metrics.json', 'w') as outfile:
            json.dump(test_metrics, fp=outfile, indent=4)

    def _save_final_adapter(self):
        output_path = self.output_dir / f'{self.adapter_name}_final_model'
        logger.info(f'Saving adapter to: {output_path}')
        self.model.save_adapter(output_path, self.adapter_name)

    def run_single_adapter_experiment(self):
        self._load_tokenizer()
        datasets_dict = self._process_datasets()
        self._load_pretrained_transformer_model()
        self._add_adapter()
        self._train_adapter(datasets_dict)
        self._save_final_adapter()
