""" Base class for data processing. Defines generic methods inherited by all datasets.
"""

from datasets import ClassLabel, Dataset
import logging
import pandas as pd
from pathlib import Path
from transformers import PreTrainedTokenizer
from typing import Dict, List, Optional, Union


logger = logging.getLogger(__name__)


class BaseDatasetProcessor:
    def __init__(self, data_dir_path: Path, text_column_name: str, task_prefix: str) -> None:
        self._data_dir: Path = data_dir_path

        self._dev_data_df: Optional[pd.DataFrame] = None
        self._train_data_df: Optional[pd.DataFrame] = None
        self._val_data_df: Optional[pd.DataFrame] = None
        self._test_data_df: Optional[pd.DataFrame] = None

        self._text_column_name: str = text_column_name
        self._task_prefix: str = task_prefix

    def create_texts_and_labels(self, task_id: Union[int, str]) -> Dict:
        """ Extract texts and labels corresponding to train/val/test splits.
        """
        train_data_df = self._train_data_df[[self._text_column_name, f'{self._task_prefix}_{task_id}']]
        train_data_df = train_data_df.dropna()
        train_texts: List[str] = train_data_df[self._text_column_name].values.tolist()
        train_labels: List[str] = train_data_df[f'{self._task_prefix}_{task_id}'].values.tolist()

        val_data_df = self._val_data_df[[self._text_column_name, f'{self._task_prefix}_{task_id}']]
        val_data_df = val_data_df.dropna()
        val_texts: List[str] = val_data_df[self._text_column_name].values.tolist()
        val_labels: List[str] = val_data_df[f'{self._task_prefix}_{task_id}'].values.tolist()

        test_data_df = self._test_data_df[[self._text_column_name, f'{self._task_prefix}_{task_id}']]
        test_data_df = test_data_df.dropna()
        test_texts: List[str] = test_data_df[self._text_column_name].values.tolist()
        test_labels: List[str] = test_data_df[f'{self._task_prefix}_{task_id}'].values.tolist()

        data_dict = {'train': {'texts': train_texts,
                               'labels': train_labels},
                     'val': {'texts': val_texts,
                             'labels': val_labels},
                     'test': {'texts': test_texts,
                              'labels': test_labels},
                     'label_set': list(set(train_labels))}
        return data_dict

    def get_datasets(self, task_id: int) -> Dict[str, Dataset]:
        """ Return transformers style datasets for each split.
        """
        data_dict = self.create_texts_and_labels(task_id=task_id)

        train_dataset = Dataset.from_dict(data_dict['train'])
        train_dataset = train_dataset.cast_column("labels", ClassLabel(num_classes=len(data_dict['label_set']),
                                                                       names=data_dict['label_set']))
        print(f'Size of train dataset: {train_dataset.num_rows}')
        print(f'Train dataset features: {train_dataset.features}')

        val_dataset = Dataset.from_dict(data_dict['val'])
        val_dataset = val_dataset.cast_column("labels", ClassLabel(num_classes=len(data_dict['label_set']),
                                                                   names=data_dict['label_set']))
        print(f'Size of val dataset: {val_dataset.num_rows}')
        print(f'Val dataset features: {val_dataset.features}')

        test_dataset = Dataset.from_dict(data_dict['test'])
        test_dataset = test_dataset.cast_column("labels", ClassLabel(num_classes=len(data_dict['label_set']),
                                                                     names=data_dict['label_set']))
        print(f'Size of test dataset: {test_dataset.num_rows}')
        print(f'Test dataset features: {test_dataset.features}')

        datasets_dict = {'train': train_dataset,
                         'val': val_dataset,
                         'test': test_dataset}
        return datasets_dict

    @staticmethod
    def encode_batch(batch, tokenizer: PreTrainedTokenizer, tokenizer_kwargs: Dict):
        """Encodes a batch of input data using the model tokenizer."""
        return tokenizer(batch["texts"], **tokenizer_kwargs)
