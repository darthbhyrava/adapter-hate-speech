""" Dataset Processor for HASOC (Hate Speech and Offensive Content Identification) 2019 dataset.
"""

import logging
from pathlib import Path
import pandas as pd
from typing import Optional

from dataset_processing.base_dataset_processor import BaseDatasetProcessor


logger = logging.getLogger(__name__)

DEFAULT_HASOC_DATA_DIR_PATH = Path(__file__).parent.parent.parent.joinpath('datasets', 'hasoc_english_dataset')
DEFAULT_HASOC_DEV_DATA_PATH = DEFAULT_HASOC_DATA_DIR_PATH / 'english_dataset.tsv'
DEFAULT_HASOC_TEST_DATA_PATH = DEFAULT_HASOC_DATA_DIR_PATH / 'hasoc2019_en_test-2019.tsv'


class HASOCDatasetProcessor(BaseDatasetProcessor):
    def __init__(self, data_dir_path: Path = DEFAULT_HASOC_DATA_DIR_PATH,
                 dev_data_path: Path = DEFAULT_HASOC_DEV_DATA_PATH,
                 test_data_path: Path = DEFAULT_HASOC_TEST_DATA_PATH,
                 validation_ratio: float = 0.2,
                 random_seed: int = 6394,
                 text_column_name: str = 'text',
                 task_prefix: str = 'task'):
        super(HASOCDatasetProcessor, self).__init__(data_dir_path=data_dir_path, text_column_name=text_column_name,
                                                    task_prefix=task_prefix)

        self._dev_data_path: Path = dev_data_path
        self._test_data_path: Path = test_data_path

        self._load_datasets()

        self._validation_ratio: float = validation_ratio
        self._random_seed: int = random_seed
        self._create_validation_split()

    def _load_datasets(self):
        """ Read the development and test dataframes
        """
        self._dev_data_df = pd.read_csv(self._dev_data_path, delimiter='\t')
        self._test_data_df = pd.read_csv(self._test_data_path, delimiter='\t')

    def _create_validation_split(self):
        """ Split the dev dataframe into train and validation
        """
        self._val_data_df = self._dev_data_df.sample(frac=self._validation_ratio, random_state=self._random_seed)
        self._train_data_df = self._dev_data_df[~self._dev_data_df.text_id.isin(self._val_data_df.text_id.values)]
