""" Dataset Processor for OLID (Offensive Language Identification) 2019 dataset.
"""

import logging
from pathlib import Path
import pandas as pd
from typing import Dict, Optional

from dataset_processing.base_dataset_processor import BaseDatasetProcessor


logger = logging.getLogger(__name__)

DEFAULT_OLID_DATA_DIR_PATH = Path(__file__).parent.parent.parent.joinpath('datasets', 'olid_english_dataset')
DEFAULT_OLID_DEV_DATA_PATH = DEFAULT_OLID_DATA_DIR_PATH / 'olid-training-v1.0.tsv'

DEFAULT_OLID_TEST_TWEETS_LEVEL_A = DEFAULT_OLID_DATA_DIR_PATH / 'testset-levela.tsv'
DEFAULT_OLID_TEST_LABELS_LEVEL_A = DEFAULT_OLID_DATA_DIR_PATH / 'labels-levela.csv'

DEFAULT_OLID_TEST_TWEETS_LEVEL_B = DEFAULT_OLID_DATA_DIR_PATH / 'testset-levelb.tsv'
DEFAULT_OLID_TEST_LABELS_LEVEL_B = DEFAULT_OLID_DATA_DIR_PATH / 'labels-levelb.csv'

DEFAULT_OLID_TEST_TWEETS_LEVEL_C = DEFAULT_OLID_DATA_DIR_PATH / 'testset-levelc.tsv'
DEFAULT_OLID_TEST_LABELS_LEVEL_C = DEFAULT_OLID_DATA_DIR_PATH / 'labels-levelc.csv'

DEFAULT_OLID_TEST_SETS_ALL_TASKS = {'a': {'tweets': DEFAULT_OLID_TEST_TWEETS_LEVEL_A,
                                          'labels': DEFAULT_OLID_TEST_LABELS_LEVEL_A},
                                    'b': {'tweets': DEFAULT_OLID_TEST_TWEETS_LEVEL_B,
                                          'labels': DEFAULT_OLID_TEST_LABELS_LEVEL_B},
                                    'c': {'tweets': DEFAULT_OLID_TEST_TWEETS_LEVEL_C,
                                          'labels': DEFAULT_OLID_TEST_LABELS_LEVEL_C}}


class OLIDDatasetProcessor(BaseDatasetProcessor):
    def __init__(self, data_dir_path: Path = DEFAULT_OLID_DATA_DIR_PATH,
                 dev_data_path: Path = DEFAULT_OLID_DEV_DATA_PATH,
                 test_data_paths_dict: Dict = DEFAULT_OLID_TEST_SETS_ALL_TASKS,
                 validation_ratio: float = 0.2,
                 random_seed: int = 6394,
                 text_column_name: str = 'tweet',
                 task_prefix: str = 'subtask'):
        super(OLIDDatasetProcessor, self).__init__(data_dir_path=data_dir_path, text_column_name=text_column_name,
                                                   task_prefix=task_prefix)

        self._dev_data_path: Path = dev_data_path
        self._test_data_paths_dict: Dict = test_data_paths_dict

        self._load_datasets()

        self._validation_ratio: float = validation_ratio
        self._random_seed: int = random_seed
        self._create_validation_split()

    def _load_datasets(self):
        """ Read the development and test dataframes
        """
        self._dev_data_df = pd.read_csv(self._dev_data_path, delimiter='\t')

        test_df_list = []
        for level in self._test_data_paths_dict:
            tweets_df = pd.read_csv(self._test_data_paths_dict[level]['tweets'], delimiter='\t')
            labels_df = pd.read_csv(self._test_data_paths_dict[level]['labels'],
                                    header=None,
                                    names=['id', f'{self._task_prefix}_{level}'])
            merged = tweets_df.merge(labels_df, on='id', how='left')
            test_df_list.append(merged)
        self._test_data_df = pd.concat(test_df_list)

    def _create_validation_split(self):
        """ Split the dev dataframe into train and validation
        """
        self._val_data_df = self._dev_data_df.sample(frac=self._validation_ratio, random_state=self._random_seed)
        self._train_data_df = self._dev_data_df[~self._dev_data_df.id.isin(self._val_data_df.id.values)]
